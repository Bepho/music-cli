//
// Created by bephomet on 6/21/18.
//
#define CRLF "\r\n"

#include <iostream>
#include <sstream>
#include <chrono>

#include "../include/communication.h"
#include "../include/communication_decl.h"

communication::communication() : port_(0), website_name_(""), socket_(0), connected_(false), terminate_(false)
{
  // start threads
  receive_thread_ = std::thread(&communication::receive, this, this);
  send_thread_ = std::thread(&communication::send, this, this);
}
communication::communication(int port, std::string website_name) : port_(port), website_name_(website_name), socket_(0), connected_(false), terminate_(false)
{
  // start threads
  receive_thread_ = std::thread(&communication::receive, this, this);
  send_thread_ = std::thread(&communication::send, this, this);

  connected_ = create_connection();
  if(connected_)
  {
    std::cout << "connection established to " + website_name_ << std::endl;
    std::cout << communication_nmsp::create_http_request(website_name_);
  }
  else
  {
    std::cout << "no connection could be estabished!" << std::endl;
  }
}
bool communication::create_connection()
{

  // AF_INET = IPv4
  if(!connected_)
  {
    socket_ = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_ < 0)
    {
      perror("ERROR can't open socket");
      return false;
    }

    server_ = gethostbyname(website_name_.c_str());
    if (server_ == nullptr)
    {
      std::cerr << "ERROR no such host" << std::endl;
      return false;
    }

    // set N bytes to null
    bzero((char *) &server_addr_, sizeof(server_addr_));

    server_addr_.sin_family = AF_INET;

    // copy N bytes of SRC to DST
    bcopy((char *) server_->h_addr,
          (char *) &server_addr_.sin_addr.s_addr,
          server_->h_length);

    // host to network byte order
    server_addr_.sin_port = htons(port_);

    // connect to server using socket and serveraddress
    if (connect(socket_, (struct sockaddr *) &server_addr_, sizeof(server_addr_)) < 0)
    {
      perror("ERROR can not connect");
      return false;
    }
  }
  else
  {
    std::cout << "connection already established";
    return false;
  }
  return true;
}
void communication::receive(communication *param)
{
  std::cout << "starting receive thread" << std::endl;
  auto parent = static_cast<communication*>(param);
  if(parent)
  {
    for(;;)
    {
      if(parent->terminate_)
      {
        break;
      }
      if(parent->connected_)
      {
        char buffer[200000];
        bzero(buffer, 200000);

        int n = read(socket_,buffer, 200000);
        if(n < 0) {
          std::string err = "ERROR could not read message: ";
          perror(err.c_str());
        }
        //TODO: react on given string
        std::cout << buffer << std::endl;

        delete buffer;
      }
      else
      {
        sleep(1);
      }
    }
  }
  std::cout << "stoping receive thread" << std::endl;
}
void communication::send(communication *param)
{
  auto parent = static_cast<communication*>(param);
  if(parent)
  {
    std::cout << "starting send thread" << std::endl;
    for(;;)
    {
      if(parent->terminate_)
      {
        break;
      }
      if(parent->connected_)
      {
        //TODO: implement queue to be sent out
        /*
        int n = write(socket_,msg.data(), msg.size());
        if(n < 0)
        {
          std::string err = "ERROR could not send message: " + msg;
          perror(err.c_str());
          return false;
        }
        std::cout << "message size: " << msg.size() << " message sent: " << n << std::endl << std::endl;
        */
      }
      else
      {
        sleep(1);
      }
    }
  }
  std::cout << "stoping send thread" << std::endl;
}

void communication::shutdown()
{
  terminate_ = true;
  if(connected_)
  {
    close(socket_);
  }
}
communication::~communication()
{
  shutdown();
}
