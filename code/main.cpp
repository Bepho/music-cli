//
// Created by bephomet on 6/21/18.
//

#include <iostream>

#include "../include/communication.h"
#include "../include/term_gui.h"

int main(void)
{
  communication comm(80,"www.youtube.com");
  term_gui gui;

  gui.init();

  for(;;)
  {

    sleep(1);
    comm.shutdown();
  }
  return 0;
} 