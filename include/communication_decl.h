//
// Created by bephomet on 6/28/18.
//

#ifndef MUSIC_CLI_COMMUNICATION_DECL_H
#define MUSIC_CLI_COMMUNICATION_DECL_H

#define CRLF "\r\n"

#include <string>
#include <sstream>


namespace communication_nmsp
{
  std::string create_http_request(std::string website_name, std::string path = "/index.html")
  {
    std::stringstream ss;
    ss << "GET " << path << " HTTP/1.1" << CRLF
       << "Host: " << website_name << CRLF
       << "Connection: close" << CRLF << CRLF;
    return ss.str();
  }
}

#endif //MUSIC_CLI_COMMUNICATION_DECL_H
