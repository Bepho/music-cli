//
// Created by bephomet on 6/21/18.
//

#ifndef MUSIC_CLI_COMMUNICATION_H
#define MUSIC_CLI_COMMUNICATION_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <thread>
#include <vector>

class communication
{
  bool connected_;
  bool terminate_;

  int port_;
  int socket_;
  std::string website_name_;
  struct hostent *server_;
  struct sockaddr_in server_addr_;

  std::thread receive_thread_;
  std::thread send_thread_;

  // threads
  void receive(communication *param);
  void send(communication *param);

  // functions

  // return: true if new connection could be estabished.
  bool create_connection(void);
  
public:
  communication();
  communication(int port, std::string website_name);
  ~communication();

  void shutdown();
};



#endif //MUSIC_CLI_COMMUNICATION_H
